defmodule SayNumberIndonesianTest do
  use ExUnit.Case
  doctest SayNumber

  describe "Say english number" do
    test "0" do
      assert SayNumber.spell(0, "id") == "Nol"
    end

    test '1_000_000_000_000_000' do
      assert_raise RuntimeError, "Angka terlalu besar", fn ->
        SayNumber.spell(1_000_000_000_000_000, "id")
      end
    end

    test "1 to 20" do
      result = Enum.map(1..20, fn num -> SayNumber.spell(num, "id") end)

      assert result == [
               "Satu",
               "Dua",
               "Tiga",
               "Empat",
               "Lima",
               "Enam",
               "Tujuh",
               "Delapan",
               "Sembilan",
               "Sepuluh",
               "Sebelas",
               "Dua belas",
               "Tiga belas",
               "Empat belas",
               "Lima belas",
               "Enam belas",
               "Tujuh belas",
               "Delapan belas",
               "Sembilan belas",
               "Dua puluh"
             ]
    end

    test "20 to 30" do
      result = Enum.map(20..30, fn num -> SayNumber.spell(num, "id") end)

      assert result == [
               "Dua puluh",
               "Dua puluh satu",
               "Dua puluh dua",
               "Dua puluh tiga",
               "Dua puluh empat",
               "Dua puluh lima",
               "Dua puluh enam",
               "Dua puluh tujuh",
               "Dua puluh delapan",
               "Dua puluh sembilan",
               "Tiga puluh"
             ]
    end

    test "60 to 70" do
      result = Enum.map(60..70, fn num -> SayNumber.spell(num, "id") end)

      assert result == [
               "Enam puluh",
               "Enam puluh satu",
               "Enam puluh dua",
               "Enam puluh tiga",
               "Enam puluh empat",
               "Enam puluh lima",
               "Enam puluh enam",
               "Enam puluh tujuh",
               "Enam puluh delapan",
               "Enam puluh sembilan",
               "Tujuh puluh"
             ]
    end

    test "90 to 99" do
      result = Enum.map(90..99, fn num -> SayNumber.spell(num, "id") end)

      assert result == [
               "Sembilan puluh",
               "Sembilan puluh satu",
               "Sembilan puluh dua",
               "Sembilan puluh tiga",
               "Sembilan puluh empat",
               "Sembilan puluh lima",
               "Sembilan puluh enam",
               "Sembilan puluh tujuh",
               "Sembilan puluh delapan",
               "Sembilan puluh sembilan"
             ]
    end

    test "100 to 125" do
      result = Enum.map(100..125, fn num -> SayNumber.spell(num, "id") end)

      assert result == [
               "Seratus",
               "Seratus satu",
               "Seratus dua",
               "Seratus tiga",
               "Seratus empat",
               "Seratus lima",
               "Seratus enam",
               "Seratus tujuh",
               "Seratus delapan",
               "Seratus sembilan",
               "Seratus sepuluh",
               "Seratus sebelas",
               "Seratus dua belas",
               "Seratus tiga belas",
               "Seratus empat belas",
               "Seratus lima belas",
               "Seratus enam belas",
               "Seratus tujuh belas",
               "Seratus delapan belas",
               "Seratus sembilan belas",
               "Seratus dua puluh",
               "Seratus dua puluh satu",
               "Seratus dua puluh dua",
               "Seratus dua puluh tiga",
               "Seratus dua puluh empat",
               "Seratus dua puluh lima"
             ]
    end

    test "190 to 200" do
      result = Enum.map(190..210, fn num -> SayNumber.spell(num, "id") end)

      assert result == [
               "Seratus sembilan puluh",
               "Seratus sembilan puluh satu",
               "Seratus sembilan puluh dua",
               "Seratus sembilan puluh tiga",
               "Seratus sembilan puluh empat",
               "Seratus sembilan puluh lima",
               "Seratus sembilan puluh enam",
               "Seratus sembilan puluh tujuh",
               "Seratus sembilan puluh delapan",
               "Seratus sembilan puluh sembilan",
               "Dua ratus",
               "Dua ratus satu",
               "Dua ratus dua",
               "Dua ratus tiga",
               "Dua ratus empat",
               "Dua ratus lima",
               "Dua ratus enam",
               "Dua ratus tujuh",
               "Dua ratus delapan",
               "Dua ratus sembilan",
               "Dua ratus sepuluh"
             ]
    end

    test "370 to 380" do
      result = Enum.map(370..380, fn num -> SayNumber.spell(num, "id") end)

      assert result == [
               "Tiga ratus tujuh puluh",
               "Tiga ratus tujuh puluh satu",
               "Tiga ratus tujuh puluh dua",
               "Tiga ratus tujuh puluh tiga",
               "Tiga ratus tujuh puluh empat",
               "Tiga ratus tujuh puluh lima",
               "Tiga ratus tujuh puluh enam",
               "Tiga ratus tujuh puluh tujuh",
               "Tiga ratus tujuh puluh delapan",
               "Tiga ratus tujuh puluh sembilan",
               "Tiga ratus delapan puluh"
             ]
    end

    test "989 to 999" do
      result = Enum.map(989..999, fn num -> SayNumber.spell(num, "id") end)

      assert result == [
               "Sembilan ratus delapan puluh sembilan",
               "Sembilan ratus sembilan puluh",
               "Sembilan ratus sembilan puluh satu",
               "Sembilan ratus sembilan puluh dua",
               "Sembilan ratus sembilan puluh tiga",
               "Sembilan ratus sembilan puluh empat",
               "Sembilan ratus sembilan puluh lima",
               "Sembilan ratus sembilan puluh enam",
               "Sembilan ratus sembilan puluh tujuh",
               "Sembilan ratus sembilan puluh delapan",
               "Sembilan ratus sembilan puluh sembilan"
             ]
    end

    test "1000 to 1015" do
      result = Enum.map(1000..1015, fn num -> SayNumber.spell(num, "id") end)

      assert result == [
               "Seribu",
               "Seribu satu",
               "Seribu dua",
               "Seribu tiga",
               "Seribu empat",
               "Seribu lima",
               "Seribu enam",
               "Seribu tujuh",
               "Seribu delapan",
               "Seribu sembilan",
               "Seribu sepuluh",
               "Seribu sebelas",
               "Seribu dua belas",
               "Seribu tiga belas",
               "Seribu empat belas",
               "Seribu lima belas"
             ]
    end

    test "thousand numbers" do
      assert SayNumber.spell(2189, "id") == "Dua ribu seratus delapan puluh sembilan"
      assert SayNumber.spell(5610, "id") == "Lima ribu enam ratus sepuluh"
      assert SayNumber.spell(9000, "id") == "Sembilan ribu"
      assert SayNumber.spell(17_000, "id") == "Tujuh belas ribu"
      assert SayNumber.spell(11_111, "id") == "Sebelas ribu seratus sebelas"
      assert SayNumber.spell(13_887, "id") == "Tiga belas ribu delapan ratus delapan puluh tujuh"
      assert SayNumber.spell(200_000, "id") == "Dua ratus ribu"
      assert SayNumber.spell(261_111, "id") == "Dua ratus enam puluh satu ribu seratus sebelas"

      assert SayNumber.spell(999_999, "id") ==
               "Sembilan ratus sembilan puluh sembilan ribu sembilan ratus sembilan puluh sembilan"

      assert SayNumber.spell(300_013, "id") == "Tiga ratus ribu tiga belas"
    end

    test "thousand numbers with special case of seribu" do
      assert SayNumber.spell(1000, "id") == "Seribu"
      assert SayNumber.spell(91_000, "id") == "Sembilan puluh satu ribu"
      assert SayNumber.spell(101_000, "id") == "Seratus satu ribu"
      assert SayNumber.spell(1_201_000, "id") == "Satu juta dua ratus satu ribu"
    end

    test "million numbers" do
      assert SayNumber.spell(1_000_000, "id") == "Satu juta"
      assert SayNumber.spell(2_000_000, "id") == "Dua juta"

      assert SayNumber.spell(5_455_103, "id") ==
               "Lima juta empat ratus lima puluh lima ribu seratus tiga"

      assert SayNumber.spell(10_000_000, "id") == "Sepuluh juta"
      assert SayNumber.spell(30_000_000, "id") == "Tiga puluh juta"
      assert SayNumber.spell(31_000_000, "id") == "Tiga puluh satu juta"

      assert SayNumber.spell(54_891_333, "id") ==
               "Lima puluh empat juta delapan ratus sembilan puluh satu ribu tiga ratus tiga puluh tiga"

      assert SayNumber.spell(100_000_000, "id") == "Seratus juta"

      assert SayNumber.spell(143_932_814, "id") ==
               "Seratus empat puluh tiga juta sembilan ratus tiga puluh dua ribu delapan ratus empat belas"
    end

    test "billion numbers" do
      assert SayNumber.spell(6_000_000_000, "id") == "Enam milyar"

      assert SayNumber.spell(8_912_541_337, "id") ==
               "Delapan milyar sembilan ratus dua belas juta lima ratus empat puluh satu ribu tiga ratus tiga puluh tujuh"

      assert SayNumber.spell(28_312_341_334, "id") ==
               "Dua puluh delapan milyar tiga ratus dua belas juta tiga ratus empat puluh satu ribu tiga ratus tiga puluh empat"

      assert SayNumber.spell(781_909_123_321, "id") ==
               "Tujuh ratus delapan puluh satu milyar sembilan ratus sembilan juta seratus dua puluh tiga ribu tiga ratus dua puluh satu"
    end

    test 'trillion numbers' do
      assert SayNumber.spell(5_000_000_000_000, "id") == "Lima trilyun"

      assert SayNumber.spell(5_899_123_164_943, "id") ==
               "Lima trilyun delapan ratus sembilan puluh sembilan milyar seratus dua puluh tiga juta seratus enam puluh empat ribu sembilan ratus empat puluh tiga"

      assert SayNumber.spell(50_000_000_000_000, "id") == "Lima puluh trilyun"

      assert SayNumber.spell(31_091_123_571_121, "id") ==
               "Tiga puluh satu trilyun sembilan puluh satu milyar seratus dua puluh tiga juta lima ratus tujuh puluh satu ribu seratus dua puluh satu"

      assert SayNumber.spell(900_000_000_000_000, "id") == "Sembilan ratus trilyun"

      assert SayNumber.spell(981_001_123_450_217, "id") ==
               "Sembilan ratus delapan puluh satu trilyun satu milyar seratus dua puluh tiga juta empat ratus lima puluh ribu dua ratus tujuh belas"
    end
  end
end
