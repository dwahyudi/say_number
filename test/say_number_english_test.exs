defmodule SayNumberEnglishTest do
  use ExUnit.Case
  doctest SayNumber

  describe "Say english number" do
    test "0" do
      assert SayNumber.spell(0) == "Zero"
    end

    test '1_000_000_000_000_000' do
      assert_raise RuntimeError, "Too large number", fn ->
        SayNumber.spell(1_000_000_000_000_000)
      end
    end

    test "1 to 20" do
      result = Enum.map(1..20, fn num -> SayNumber.spell(num) end)

      assert result == [
               "One",
               "Two",
               "Three",
               "Four",
               "Five",
               "Six",
               "Seven",
               "Eight",
               "Nine",
               "Ten",
               "Eleven",
               "Twelve",
               "Thirteen",
               "Fourteen",
               "Fifteen",
               "Sixteen",
               "Seventeen",
               "Eighteen",
               "Nineteen",
               "Twenty"
             ]
    end

    test "20 to 30" do
      result = Enum.map(20..30, fn num -> SayNumber.spell(num) end)

      assert result == [
               "Twenty",
               "Twenty one",
               "Twenty two",
               "Twenty three",
               "Twenty four",
               "Twenty five",
               "Twenty six",
               "Twenty seven",
               "Twenty eight",
               "Twenty nine",
               "Thirty"
             ]
    end

    test "60 to 70" do
      result = Enum.map(60..70, fn num -> SayNumber.spell(num) end)

      assert result == [
               "Sixty",
               "Sixty one",
               "Sixty two",
               "Sixty three",
               "Sixty four",
               "Sixty five",
               "Sixty six",
               "Sixty seven",
               "Sixty eight",
               "Sixty nine",
               "Seventy"
             ]
    end

    test "90 to 99" do
      result = Enum.map(90..99, fn num -> SayNumber.spell(num) end)

      assert result == [
               "Ninety",
               "Ninety one",
               "Ninety two",
               "Ninety three",
               "Ninety four",
               "Ninety five",
               "Ninety six",
               "Ninety seven",
               "Ninety eight",
               "Ninety nine"
             ]
    end

    test "100 to 125" do
      result = Enum.map(100..125, fn num -> SayNumber.spell(num) end)

      assert result == [
               "One hundred",
               "One hundred one",
               "One hundred two",
               "One hundred three",
               "One hundred four",
               "One hundred five",
               "One hundred six",
               "One hundred seven",
               "One hundred eight",
               "One hundred nine",
               "One hundred ten",
               "One hundred eleven",
               "One hundred twelve",
               "One hundred thirteen",
               "One hundred fourteen",
               "One hundred fifteen",
               "One hundred sixteen",
               "One hundred seventeen",
               "One hundred eighteen",
               "One hundred nineteen",
               "One hundred twenty",
               "One hundred twenty one",
               "One hundred twenty two",
               "One hundred twenty three",
               "One hundred twenty four",
               "One hundred twenty five"
             ]
    end

    test "190 to 200" do
      result = Enum.map(190..210, fn num -> SayNumber.spell(num) end)

      assert result == [
               "One hundred ninety",
               "One hundred ninety one",
               "One hundred ninety two",
               "One hundred ninety three",
               "One hundred ninety four",
               "One hundred ninety five",
               "One hundred ninety six",
               "One hundred ninety seven",
               "One hundred ninety eight",
               "One hundred ninety nine",
               "Two hundred",
               "Two hundred one",
               "Two hundred two",
               "Two hundred three",
               "Two hundred four",
               "Two hundred five",
               "Two hundred six",
               "Two hundred seven",
               "Two hundred eight",
               "Two hundred nine",
               "Two hundred ten"
             ]
    end

    test "370 to 380" do
      result = Enum.map(370..380, fn num -> SayNumber.spell(num) end)

      assert result == [
               "Three hundred seventy",
               "Three hundred seventy one",
               "Three hundred seventy two",
               "Three hundred seventy three",
               "Three hundred seventy four",
               "Three hundred seventy five",
               "Three hundred seventy six",
               "Three hundred seventy seven",
               "Three hundred seventy eight",
               "Three hundred seventy nine",
               "Three hundred eighty"
             ]
    end

    test "989 to 999" do
      result = Enum.map(989..999, fn num -> SayNumber.spell(num) end)

      assert result == [
               "Nine hundred eighty nine",
               "Nine hundred ninety",
               "Nine hundred ninety one",
               "Nine hundred ninety two",
               "Nine hundred ninety three",
               "Nine hundred ninety four",
               "Nine hundred ninety five",
               "Nine hundred ninety six",
               "Nine hundred ninety seven",
               "Nine hundred ninety eight",
               "Nine hundred ninety nine"
             ]
    end

    test "1000 to 1015" do
      result = Enum.map(1000..1015, fn num -> SayNumber.spell(num) end)

      assert result == [
               "One thousand",
               "One thousand one",
               "One thousand two",
               "One thousand three",
               "One thousand four",
               "One thousand five",
               "One thousand six",
               "One thousand seven",
               "One thousand eight",
               "One thousand nine",
               "One thousand ten",
               "One thousand eleven",
               "One thousand twelve",
               "One thousand thirteen",
               "One thousand fourteen",
               "One thousand fifteen"
             ]
    end

    test "some thousand numbers" do
      assert SayNumber.spell(1899) == "One thousand eight hundred ninety nine"
      assert SayNumber.spell(3091) == "Three thousand ninety one"
      assert SayNumber.spell(6111) == "Six thousand one hundred eleven"
      assert SayNumber.spell(7000) == "Seven thousand"
      assert SayNumber.spell(8888) == "Eight thousand eight hundred eighty eight"
      assert SayNumber.spell(1991) == "One thousand nine hundred ninety one"
      assert SayNumber.spell(6556) == "Six thousand five hundred fifty six"
      assert SayNumber.spell(7772) == "Seven thousand seven hundred seventy two"
      assert SayNumber.spell(9999) == "Nine thousand nine hundred ninety nine"
      assert SayNumber.spell(3112) == "Three thousand one hundred twelve"

      assert SayNumber.spell(19_999) == "Nineteen thousand nine hundred ninety nine"
      assert SayNumber.spell(10_000) == "Ten thousand"
      assert SayNumber.spell(80_000) == "Eighty thousand"
      assert SayNumber.spell(54_321) == "Fifty four thousand three hundred twenty one"
      assert SayNumber.spell(88_888) == "Eighty eight thousand eight hundred eighty eight"
      assert SayNumber.spell(30_303) == "Thirty thousand three hundred three"
      assert SayNumber.spell(65_018) == "Sixty five thousand eighteen"

      assert SayNumber.spell(123_456) ==
               "One hundred twenty three thousand four hundred fifty six"

      assert SayNumber.spell(746_888) ==
               "Seven hundred forty six thousand eight hundred eighty eight"

      assert SayNumber.spell(555_555) ==
               "Five hundred fifty five thousand five hundred fifty five"

      assert SayNumber.spell(999_010) == "Nine hundred ninety nine thousand ten"
      assert SayNumber.spell(800_000) == "Eight hundred thousand"
      assert SayNumber.spell(200_014) == "Two hundred thousand fourteen"
    end

    test 'some million numbers' do
      assert SayNumber.spell(1_000_000) == "One million"

      assert SayNumber.spell(1_111_111) ==
               "One million one hundred eleven thousand one hundred eleven"

      assert SayNumber.spell(5_000_020) == "Five million twenty"
      assert SayNumber.spell(9_010_209) == "Nine million ten thousand two hundred nine"
      assert SayNumber.spell(3_333_000) == "Three million three hundred thirty three thousand"

      assert SayNumber.spell(10_234_567) ==
               "Ten million two hundred thirty four thousand five hundred sixty seven"

      assert SayNumber.spell(90_000_000) == "Ninety million"

      assert SayNumber.spell(37_888_000) ==
               "Thirty seven million eight hundred eighty eight thousand"

      assert SayNumber.spell(400_000_000) == "Four hundred million"

      assert SayNumber.spell(987_654_312) ==
               "Nine hundred eighty seven million six hundred fifty four thousand three hundred twelve"

      assert SayNumber.spell(100_100_100) ==
               "One hundred million one hundred thousand one hundred"
    end

    test 'some billion numbers' do
      assert SayNumber.spell(1_100_100_100) ==
               "One billion one hundred million one hundred thousand one hundred"

      assert SayNumber.spell(3_000_000_000) == "Three billion"
      assert SayNumber.spell(9_000_000_011) == "Nine billion eleven"

      assert SayNumber.spell(2_468_246_824) ==
               "Two billion four hundred sixty eight million two hundred forty six thousand eight hundred twenty four"

      assert SayNumber.spell(30_000_000_000) == "Thirty billion"
      assert SayNumber.spell(50_000_001_119) == "Fifty billion one thousand one hundred nineteen"

      assert SayNumber.spell(98_123_456_789) ==
               "Ninety eight billion one hundred twenty three million four hundred fifty six thousand seven hundred eighty nine"

      assert SayNumber.spell(400_000_000_000) == "Four hundred billion"

      assert SayNumber.spell(398_923_456_710) ==
               "Three hundred ninety eight billion nine hundred twenty three million four hundred fifty six thousand seven hundred ten"
    end

    test 'some trillion numbers' do
      assert SayNumber.spell(2_000_000_000_000) == "Two trillion"

      assert SayNumber.spell(3_445_981_123_546) ==
               "Three trillion four hundred forty five billion nine hundred eighty one million one hundred twenty three thousand five hundred forty six"

      assert SayNumber.spell(99_999_999_111_012) ==
               "Ninety nine trillion nine hundred ninety nine billion nine hundred ninety nine million one hundred eleven thousand twelve"

      assert SayNumber.spell(500_000_000_000_000) == "Five hundred trillion"

      assert SayNumber.spell(999_888_777_222_112) ==
               "Nine hundred ninety nine trillion eight hundred eighty eight billion seven hundred seventy seven million two hundred twenty two thousand one hundred twelve"
    end
  end
end
