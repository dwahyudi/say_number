defmodule SayNumber.Gettext do
  @moduledoc false

  use Gettext, otp_app: :say_number
end
