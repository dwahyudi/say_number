defmodule SayNumber.Patcher.Indonesian do
  @moduledoc false

  def perform(string) do
    do_fix_indonesian_in_order_infix(string)
  end

  # Fix something like 'satu ratus' to 'seratus' in some cases.
  defp do_fix_indonesian_in_order_infix(string) do
    string
    |> String.replace("satu ratus", "seratus")
    |> String.replace("satu ribu", "seribu")
    |> String.replace("ratus seribu", "ratus satu ribu")
    |> String.replace("puluh seribu", "puluh satu ribu")
  end
end
