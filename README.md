# SayNumber


Simple library for spelling numbers into English or Indonesian language (Bahasa Indonesia).

```elixir
iex> SayNumber.spell(1899)
"One thousand eight hundred ninety nine"

iex> SayNumber.spell(91_000, "id")
"Sembilan puluh satu ribu"
```


## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed
by adding `say_number` to your list of dependencies in `mix.exs`:

```elixir
def deps do
  [
    {:say_number, "~> 0.0.6"}
  ]
end
```

Documentation can be generated with [ExDoc](https://github.com/elixir-lang/ex_doc)
and published on [HexDocs](https://hexdocs.pm). Once published, the docs can
be found at [https://hexdocs.pm/say_number](https://hexdocs.pm/say_number).

