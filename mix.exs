defmodule SayNumber.MixProject do
  use Mix.Project

  def project do
    [
      app: :say_number,
      version: "0.0.6",
      elixir: "~> 1.7",
      start_permanent: Mix.env() == :prod,
      deps: deps(),
      compilers: [:gettext] ++ Mix.compilers(),
      description: description(),
      package: package()
    ]
  end

  # Run "mix help compile.app" to learn about applications.
  def application do
    [
      extra_applications: [:logger]
    ]
  end

  # Run "mix help deps" to learn about dependencies.
  defp deps do
    [
      # {:dep_from_hexpm, "~> 0.3.0"},
      # {:dep_from_git, git: "https://github.com/elixir-lang/my_dep.git", tag: "0.1.0"},
      {:credo, "~> 1.0.0", only: [:dev, :test], runtime: false},
      {:gettext, ">= 0.0.0"},
      {:ex_doc, "~> 0.19", only: :dev}
    ]
  end

  defp description do
    """
    Simple library for spelling numbers into English or Indonesian language (Bahasa Indonesia).
    """
  end

  defp package do
    [
      files: ["lib", "priv", "mix.exs", "README*", "LICENSE*"],
      maintainers: ["Dwi Wahyudi"],
      licenses: ["MIT"],
      links: %{"Gitlab" => "https://gitlab.com/dwahyudi/say_number"}
    ]
  end
end
